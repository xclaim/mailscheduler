package com.bni.mailscheduler.service;

import com.bni.mailscheduler.dto.Receipt;

import java.util.List;

public interface GetReceipt {

    public List<Receipt> getReceipt();
    public boolean updateInventory(String cif);
}
