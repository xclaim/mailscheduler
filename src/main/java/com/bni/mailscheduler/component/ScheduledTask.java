package com.bni.mailscheduler.component;
import com.bni.mailscheduler.dto.Receipt;
import com.bni.mailscheduler.service.GetReceipt;
import com.bni.mailscheduler.util.SoapUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.xml.soap.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component("scheduledTask")
public class ScheduledTask {

    @Value("${soap.url.getreciept}")
    private String getReceiptUrl;

    @Value("${soap.url.sendmail}")
    private String sendMailUrl;

    @Autowired
    private GetReceipt getReceipt;

    private final Logger log = LoggerFactory.getLogger(ScheduledTask.class);

    @Scheduled(cron = "${cron.expression}")
    public void sendMail(){
        try{
            //TODO get receipt from database, make SOAP message, call mock server, log everything
            ObjectMapper obj = new ObjectMapper();
            List<Receipt> receiptList = getReceipt.getReceipt();
            //List<Receipt> receiptList = getDummyReceipt();
            SOAPConnection soapConnection = SOAPConnectionFactory.newInstance().createConnection();
            if(!receiptList.isEmpty()){
                for (Receipt receipt : receiptList) {
                    SOAPMessage request = createReceiptMessageRequest(obj.writeValueAsString(receipt));
                    logPayload(getReceiptUrl, SoapUtil.flushToString(request));
                    SOAPMessage response = soapConnection.call(request, getReceiptUrl);
                    if (response != null) {
                        log.info("Response Payload :" + SoapUtil.flushToString(response));
                        org.w3c.dom.Node node = response.getSOAPBody().getElementsByTagName("receipt").item(0);
                        String content = node != null ? node.getTextContent() : "";
                        if (!content.isEmpty()) {
                            SOAPMessage sendRequest = createSendMessageRequest(
                                    "<![CDATA["+content+"]]>" ,
                                    "new-mobile-banking@bni.co.id",
                                    receipt.getEmail(),
                                    "Pembukaan Rekening BNI");
                            logPayload(sendMailUrl, SoapUtil.flushToString(sendRequest));
                            SOAPMessage responseSend = soapConnection.call(sendRequest, sendMailUrl);
                            //log.info("Response Payload :" + SoapUtil.flushToString(responseSend));
                            //if (responseSend != null) {
                                log.info("Sukses kirim pesan");
                                boolean updateStatus = getReceipt.updateInventory(receipt.getCif());
                                if (updateStatus) {
                                    log.info("email status for cif " + receipt.getCif() + " updated");
                                } else {
                                    log.error("failed update email status for cif " + receipt.getCif());
                                }
                            //} else {
                            //    log.error("error kirim pesan");
                            //}
                        } else {
                            org.w3c.dom.Node faultCodeNode = response.getSOAPBody().getElementsByTagName("faultcode").item(0);
                            org.w3c.dom.Node faultMessageNode = response.getSOAPBody().getElementsByTagName("faultstring").item(0);
                            String faultCode = faultCodeNode != null ? faultCodeNode.getTextContent() : "";
                            String faultMessage = faultMessageNode != null ? faultMessageNode.getTextContent():"";
                            log.error("ERROR fault code : "+faultCode+"\nFaultMessage : "+faultMessage);
                        }
                    } else {
                        log.error("response not found");
                    }

                }
            }else {
                log.error("data not found");
            }
        }catch(Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    private SOAPMessage createReceiptMessageRequest(String receiptJson) throws Exception{
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
        SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();

        HashMap<String,String> attribute = new HashMap<String,String>();
        attribute.put("rec","http://service.bni.co.id/receipt");
        attribute.put("obj","http://service.bni.co.id/receipt/obj");
        SoapUtil.addEnvelopeNamespace(soapEnvelope,attribute);

        SOAPHeader soapHeader = soapEnvelope.getHeader();
        SOAPBody soapBody = soapEnvelope.getBody();

        SOAPElement compose = SoapUtil.addChild(soapBody,"compose","rec");
        SOAPElement request = SoapUtil.addChild(compose,"request","rec");
        SoapUtil.addChildWithValue(request,"channelId","obj","EFORM");
        SoapUtil.addChildWithValue(request,"jsonStringParams","obj",receiptJson);
        SoapUtil.addChildWithValue(request,"providerId","obj","BRANCHLESS_OPENACCOUNT_RECEIPT");

        soapMessage.saveChanges();
        return soapMessage;
    }

    private SOAPMessage createSendMessageRequest(String html, String from, String to, String subject) throws Exception{
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
        SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();

        HashMap<String,String> attribute = new HashMap<String,String>();
        attribute.put("mail","http://service.bni.co.id/mail");
        SoapUtil.addEnvelopeNamespace(soapEnvelope,attribute);

        SOAPHeader soapHeader = soapEnvelope.getHeader();
        SOAPBody soapBody = soapEnvelope.getBody();

        SOAPElement send = SoapUtil.addChild(soapBody,"send","mail");
        SOAPElement emailOut = SoapUtil.addChild(send,"emailOut");
        SoapUtil.addChildWithValue(emailOut,"from",from);
        SoapUtil.addChildWithValue(emailOut,"to",to);
        SoapUtil.addChildWithValue(emailOut,"subject",subject);
        SoapUtil.addChildWithValue(emailOut,"content",html);

        soapMessage.saveChanges();
        return soapMessage;
    }

    private void logPayload(String endpoint, String payload){
        log.info("send request to");
        log.info("Endpoint :"+endpoint);
        log.info("Payload :"+payload);
    }

    private List<Receipt> getDummyReceipt(){
        List<Receipt> receiptList = new ArrayList<>();
        Receipt receipt = new Receipt();
        receipt.setCustName("IIN KAPRIATI");
        receipt.setRefNum("20211125140852525737");
        receipt.setRegisterDate("25 November 2021");
        receipt.setRegisterTime("14:08:52 WIB");
        receipt.setNewAccountNum("1000115378");
        receipt.setAccountType("BNI Taplus");
        receipt.setCardNum("5264229990009669");
        receipt.setAccountBranch("KCP MABES TNI CILANGKAP");
        receipt.setBranchName("KCP MABES TNI CILANGKAP");
        receipt.setAccStatus("Gagal Membuat Pin");
        receipt.setInitialDeposit("250.000,-");
        receipt.setLastDepositDate("25 Desember 2021");
        receipt.setEmail("iinkapriati2@gmail.com");
        receipt.setCif("9100741778");
        receiptList.add(receipt);
        return receiptList;
    }
}
