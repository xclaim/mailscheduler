package com.bni.mailscheduler.util;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.InputSource;

import javax.xml.transform.sax.SAXSource;

public class SoapUtil {

    public static void addEnvelopeNamespace(SOAPEnvelope envelope, HashMap<String,String> attributes)throws Exception{
        for(String key : attributes.keySet()){
            envelope.addNamespaceDeclaration(key,attributes.get(key));
        }
    }

    //body
    public static SOAPElement addChild(SOAPBody body, String childName) throws Exception{
        return body.addChildElement(childName);
    }

    public static SOAPElement addChild(SOAPBody body, String childName, String schema) throws Exception{
        return body.addChildElement(childName, schema);
    }

    public static SOAPElement addChildWithValue(SOAPBody body, String childName,String value) throws Exception{
        return body.addChildElement(childName).addTextNode(value);
    }

    public static SOAPElement addChildWithValue(SOAPBody body, String childName, String schema, String value) throws Exception{
        return body.addChildElement(childName, schema).addTextNode(value);
    }

    public static SOAPElement addChildWithAttribute(SOAPBody body, SOAPEnvelope envelope, String childName, String attrName, String attrValue) throws Exception{
        return body.addChildElement(childName).addAttribute(envelope.createName(attrName),attrValue);
    }

    public static SOAPElement addChildWithAttribute(SOAPBody body, SOAPEnvelope envelope, String childName, String attrName, String attrValue, String schema) throws Exception{
        return body.addChildElement(childName,schema).addAttribute(envelope.createName(attrName),attrValue);
    }

    public static SOAPElement addChildWithAttribute(SOAPBody body, SOAPEnvelope envelope, String childName, HashMap<String, String> attributes) throws Exception{
        SOAPElement element = body.addChildElement(childName);
        for(String key : attributes.keySet()){
            element.addAttribute(envelope.createName(key),attributes.get(key));
        }
        return element;
    }

    public static SOAPElement addChildWithAttribute(SOAPBody body, SOAPEnvelope envelope, String childName, HashMap<String, String> attributes, String schema) throws Exception{
        SOAPElement element = body.addChildElement(childName,schema);
        for(String key : attributes.keySet()){
            element.addAttribute(envelope.createName(key),attributes.get(key));
        }
        return element;
    }

    public static SOAPElement addChildWithAttributeAndValue(SOAPBody body, SOAPEnvelope envelope, String childName, String attrName, String attrValue, String value) throws Exception{
        return body.addChildElement(childName).addAttribute(envelope.createName(attrName),attrValue).addTextNode(value);
    }

    public static SOAPElement addChildWithAttributeAndValue(SOAPBody body, SOAPEnvelope envelope, String childName, String attrName, String attrValue, String schema, String value) throws Exception{
        return body.addChildElement(childName,schema).addAttribute(envelope.createName(attrName),attrValue).addTextNode(value);
    }

    public static SOAPElement addChildWithAttributeAndValue(SOAPBody body, SOAPEnvelope envelope, String childName, HashMap<String, String> attributes, String value) throws Exception{
        SOAPElement element = body.addChildElement(childName);
        for(String key : attributes.keySet()){
            element.addAttribute(envelope.createName(key),attributes.get(key));
        }
        element.addTextNode(value);
        return element;
    }

    public static SOAPElement addChildWithAttributeAndValue(SOAPBody body, SOAPEnvelope envelope, String childName, HashMap<String, String> attributes, String schema, String value) throws Exception{
        SOAPElement element = body.addChildElement(childName,schema);
        for(String key : attributes.keySet()){
            element.addAttribute(envelope.createName(key),attributes.get(key));
        }
        element.addTextNode(value);
        return element;
    }

    //element
    public static SOAPElement addChild(SOAPElement body, String childName) throws Exception{
        return body.addChildElement(childName);
    }

    public static SOAPElement addChild(SOAPElement body, String childName, String schema) throws Exception{
        return body.addChildElement(childName, schema);

    }

    public static SOAPElement addChildWithValue(SOAPElement body, String childName,String value) throws Exception{
        return body.addChildElement(childName).addTextNode(value);

    }

    public static SOAPElement addChildWithValue(SOAPElement body, String childName, String schema, String value) throws Exception{
        return body.addChildElement(childName, schema).addTextNode(value);

    }

    public static SOAPElement addChildWithAttribute(SOAPElement body, SOAPEnvelope envelope, String childName, String attrName, String attrValue) throws Exception{
        return body.addChildElement(childName).addAttribute(envelope.createName(attrName),attrValue);
    }

    public static SOAPElement addChildWithAttribute(SOAPElement body, SOAPEnvelope envelope, String childName, String attrName, String attrValue, String schema) throws Exception{
        return body.addChildElement(childName,schema).addAttribute(envelope.createName(attrName),attrValue);
    }

    public static SOAPElement addChildWithAttribute(SOAPElement body, SOAPEnvelope envelope, String childName, HashMap<String, String> attributes) throws Exception{
        SOAPElement element = body.addChildElement(childName);
        for(String key : attributes.keySet()){
            element.addAttribute(envelope.createName(key),attributes.get(key));
        }
        return element;
    }

    public static SOAPElement addChildWithAttribute(SOAPElement body, SOAPEnvelope envelope, String childName, HashMap<String, String> attributes, String schema) throws Exception{
        SOAPElement element = body.addChildElement(childName,schema);
        for(String key : attributes.keySet()){
            element.addAttribute(envelope.createName(key),attributes.get(key));
        }
        return element;
    }

    public static SOAPElement addChildWithAttributeAndValue(SOAPElement body, SOAPEnvelope envelope, String childName, String attrName, String attrValue, String value) throws Exception{
        return body.addChildElement(childName).addAttribute(envelope.createName(attrName),attrValue).addTextNode(value);
    }

    public static SOAPElement addChildWithAttributeAndValue(SOAPElement body, SOAPEnvelope envelope, String childName, String attrName, String attrValue, String schema, String value) throws Exception{
        return body.addChildElement(childName,schema).addAttribute(envelope.createName(attrName),attrValue).addTextNode(value);
    }

    public static SOAPElement addChildWithAttributeAndValue(SOAPElement body, SOAPEnvelope envelope, String childName, HashMap<String, String> attributes, String value) throws Exception{
        SOAPElement element = body.addChildElement(childName);
        for(String key : attributes.keySet()){
            element.addAttribute(envelope.createName(key),attributes.get(key));
        }
        element.addTextNode(value);
        return element;
    }

    public static SOAPElement addChildWithAttributeAndValue(SOAPElement body, SOAPEnvelope envelope, String childName, HashMap<String, String> attributes, String schema, String value) throws Exception{
        SOAPElement element = body.addChildElement(childName,schema);
        for(String key : attributes.keySet()){
            element.addAttribute(envelope.createName(key),attributes.get(key));
        }
        element.addTextNode(value);
        return element;
    }

    public static String flushToString(SOAPMessage message) throws Exception{
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        message.writeTo(outStream);
        String soapXml = new String(outStream.toByteArray());
        outStream.close();
        return soapXml;
    }

    public static String formatXmlString(String xml) throws Exception{
        Transformer serializer= SAXTransformerFactory.newInstance().newTransformer();
        serializer.setOutputProperty(OutputKeys.INDENT, "yes");
        serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        //serializer.setOutputProperty("{http://xml.customer.org/xslt}indent-amount", "2");
        Source xmlSource=new SAXSource(new InputSource(new ByteArrayInputStream(xml.getBytes())));
        StreamResult res =  new StreamResult(new ByteArrayOutputStream());
        serializer.transform(xmlSource, res);
        return new String(((ByteArrayOutputStream)res.getOutputStream()).toByteArray());
    }
}
